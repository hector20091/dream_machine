var DM = function () {
    var contactMapId = '#contact-map';
    var coordinates = [];

    return {
        init: function () {
            DM.initMap();
        },

        initMap: function () {
            var $map = $(contactMapId);

            if($map.length) {
                var coords = $map.data('coords');

                if(coords.length) {
                    $.each(coords, function (i,e) {
                        coordinates.push(e.map_address);
                    });

                    DM.renderMap();
                }
            }
        },

        renderMap: function () {
           var mapOptions = {
               zoom: 15,
               mapTypeId: google.maps.MapTypeId.ROADMAP,
               /*styles:[
                   {
                       "featureType": "water",
                       "elementType": "geometry",
                       "stylers": [
                           {
                               "color": "#e9e9e9"
                           },
                           {
                               "lightness": 17
                           }
                       ]
                   },
                   {
                       "featureType": "landscape",
                       "elementType": "geometry",
                       "stylers": [
                           {
                               "color": "#f5f5f5"
                           },
                           {
                               "lightness": 20
                           }
                       ]
                   },
                   {
                       "featureType": "road.highway",
                       "elementType": "geometry.fill",
                       "stylers": [
                           {
                               "color": "#ffffff"
                           },
                           {
                               "lightness": 17
                           }
                       ]
                   },
                   {
                       "featureType": "road.highway",
                       "elementType": "geometry.stroke",
                       "stylers": [
                           {
                               "color": "#ffffff"
                           },
                           {
                               "lightness": 29
                           },
                           {
                               "weight": 0.2
                           }
                       ]
                   },
                   {
                       "featureType": "road.arterial",
                       "elementType": "geometry",
                       "stylers": [
                           {
                               "color": "#ffffff"
                           },
                           {
                               "lightness": 18
                           }
                       ]
                   },
                   {
                       "featureType": "road.local",
                       "elementType": "geometry",
                       "stylers": [
                           {
                               "color": "#ffffff"
                           },
                           {
                               "lightness": 16
                           }
                       ]
                   },
                   {
                       "featureType": "poi",
                       "elementType": "geometry",
                       "stylers": [
                           {
                               "color": "#f5f5f5"
                           },
                           {
                               "lightness": 21
                           }
                       ]
                   },
                   {
                       "featureType": "poi.park",
                       "elementType": "geometry",
                       "stylers": [
                           {
                               "color": "#dedede"
                           },
                           {
                               "lightness": 21
                           }
                       ]
                   },
                   {
                       "elementType": "labels.text.stroke",
                       "stylers": [
                           {
                               "visibility": "on"
                           },
                           {
                               "color": "#ffffff"
                           },
                           {
                               "lightness": 16
                           }
                       ]
                   },
                   {
                       "elementType": "labels.text.fill",
                       "stylers": [
                           {
                               "saturation": 36
                           },
                           {
                               "color": "#333333"
                           },
                           {
                               "lightness": 40
                           }
                       ]
                   },
                   {
                       "elementType": "labels.icon",
                       "stylers": [
                           {
                               "visibility": "off"
                           }
                       ]
                   },
                   {
                       "featureType": "transit",
                       "elementType": "geometry",
                       "stylers": [
                           {
                               "color": "#f2f2f2"
                           },
                           {
                               "lightness": 19
                           }
                       ]
                   },
                   {
                       "featureType": "administrative",
                       "elementType": "geometry.fill",
                       "stylers": [
                           {
                               "color": "#fefefe"
                           },
                           {
                               "lightness": 20
                           }
                       ]
                   },
                   {
                       "featureType": "administrative",
                       "elementType": "geometry.stroke",
                       "stylers": [
                           {
                               "color": "#fefefe"
                           },
                           {
                               "lightness": 17
                           },
                           {
                               "weight": 1.2
                           }
                       ]
                   }
               ]*/
           };

           var contactMap = new google.maps.Map(document.getElementById(contactMapId.replace('#','')), mapOptions);

           var infowindow = new google.maps.InfoWindow();
           var marker, i;
           var latlngbounds = new google.maps.LatLngBounds();

           for (i = 0; i < coordinates.length; i++) {

               var image = {
                   url: dmConfig.map_marker,
                   // This marker is 60 pixels wide by 83 pixels high.
                   size: new google.maps.Size(28, 42),
                   // The origin for this image is (0, 0).
                   origin: new google.maps.Point(0, 0)
               };

               marker = new google.maps.Marker({
                   position: new google.maps.LatLng(coordinates[i]['lat'], coordinates[i]['lng']),
                   icon: image,
                   map: contactMap
               });

               google.maps.event.addListener(marker, 'click', (function(marker, i) {
                   return function() {
                       infowindow.setContent(coordinates[i]['address']);
                       infowindow.open(contactMap, marker);
                   }
               })(marker, i));

               latlngbounds.extend(new google.maps.LatLng(coordinates[i]['lat'], coordinates[i]['lng']));
           }

            contactMap.fitBounds(latlngbounds);
           new google.maps.Rectangle({
               bounds: latlngbounds,
               map: contactMap,
               fillColor: "#000000",
               fillOpacity: 0,
               strokeWeight: 0
           });

        }
    }
}();

$(window).on('load', function () {
    DM.init();
});