<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="preloader">
    <div class="dizzy-gillespie"></div>
</div><!-- /.preloader -->

<div class="boxed">

    <section id="header" class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="logo" class="logo float-left">
                        <a href="<?php bloginfo('url') ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.jpg" width="60">
                        </a>
                    </div><!-- /.logo -->
                    <div class="get-quote float-right">
                        <a href="<?php echo get_the_permalink(38); ?>" title="">Связаться</a>
                    </div>
                    <div class="nav-wrap">
                        <div class="btn-menu">
                            <span></span>
                        </div><!-- //mobile menu button -->
                        <div id="mainnav" class="mainnav">

                            <?php wp_nav_menu(array(
                                'theme_location' => 'top',
                                'container' => ''
                            )) ?>

                        </div><!-- /.mainnav -->
                    </div><!-- /.nav-wrap -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section><!-- /.header -->