<?php $section = $args['section'];

$title = $section['faq_title'];
$faqs_list = $section['faqs_list'];
$content_image_url = $section['faq_content_image_url'];

$content_class = 'col-xs-12';
if(!empty($content_image_url)) {
    $content_class = 'col-md-6';
}

?>

<section class="flat-row">
    <div class="container">
        <div class="row">
            <div class="<?php echo $content_class; ?>">

                <?php if(!empty($title)) { ?>

                    <div class="flat-title left">
                        <h3 class="font-weight-3"><?php echo $title; ?></h3>
                    </div>

                <?php } ?>

                <?php if(!empty($faqs_list) && sizeof($faqs_list) > 0) { ?>

                    <div class="accordion">

                        <?php foreach ($faqs_list as $key => $faq_item) { ?>

                            <div class="accordion-toggle">
                                <div class="toggle-title <?php echo ($key==0)?'active':''; ?>"><?php echo $faq_item['question']; ?></div>
                                <div class="toggle-content">

                                    <?php echo $faq_item['answer']; ?>

                                    <div class="clearfix"></div>
                                </div>
                            </div><!-- /.accordion-toggle -->

                        <?php } ?>

                    </div><!-- /.accordions -->

                <?php } ?>

            </div><!-- /.col-md-6 -->

            <?php if(!empty($content_image_url)) { ?>

                <div class="col-md-6">
                    <div class="history-video">
                        <div class="flat-video-fancybox">
                            <img src="<?php echo $content_image_url; ?>" class="border-radius-5">
                        </div>
                    </div><!-- /.history-video -->
                </div><!-- /.col-md-6 -->

            <?php } ?>

        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-row -->