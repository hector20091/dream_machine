<section class="flat-tabs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-about style2 v1">
                    <ul class="tab-list-about">
                        <li class="active tab-item active">
                            <div class="box-header background no-border">
                                <div class="icon">
                                    <span class="ti-ruler-pencil"></span>
                                </div>
                                <div class="box-title">
                                    <h6>Многолетний опыт</h6>
                                </div>
                            </div>
                        </li><!-- /.tab-item -->
                        <li class="tab-item">
                            <div class="box-header background no-border">
                                <div class="icon">
                                    <span class="ti-dashboard"></span>
                                </div>
                                <div class="box-title">
                                    <h6>Авто «под ключ»</h6>
                                </div>
                            </div>
                        </li><!-- /.tab-item -->
                        <li class="tab-item">
                            <div class="box-header background no-border">
                                <div class="icon">
                                    <span class="ti-settings"></span>
                                </div>
                                <div class="box-title">
                                    <h6>Услуга финансирования</h6>
                                </div>
                            </div>
                        </li><!-- /.tab-item -->
                        <li class="tab-item">
                            <div class="box-header background no-border">
                                <div class="icon">
                                    <span class="ti-headphone-alt"></span>
                                </div>
                                <div class="box-title">
                                    <h6>Связь 24/7</h6>
                                </div>
                            </div>
                        </li><!-- /.tab-item -->
                        <li class="tab-item">
                            <div class="box-header background no-border">
                                <div class="icon">
                                    <span class="ti-alarm-clock"></span>
                                </div>
                                <div class="box-title">
                                    <h6>Прозрачное сотрудничество</h6>
                                </div>
                            </div>
                        </li><!-- /.tab-item -->
                    </ul><!-- /.tab-list-about -->
                    <div class="content-tab">
                        <div class="content-inner overflow">
                            <div class="one-half">
                                <div class="text-tabs">
                                    <h5>DreamMachine</h5>
                                    <div class="text-content">
                                        <p>Dream Machine – это лучший выбор для покупки авто в США и Грузии!</p>
                                        <p>Dream Machine – компания опытных специалистов, которые увлечены своим делом и знают все про автомобили.</p>
                                        <p>Заказывая авто у нас, Вы можете быть уверены в том, что мы подберем для Вас лучший вариант по выгодной для Вас цене.</p>
                                    </div>
                                </div><!-- /.text-tabs -->
                            </div><!-- /.one-half -->
                            <div class="one-half">
                                <ul class="padding-left-100">
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Импортируем авто из Америки уже более 10 лет</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Имеем доступ как к открытым, так и к закрытым аукционам США</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Выполняем все работы «под ключ»</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Мы всегда на связи и готовы ответить на все Ваши вопросы</li>
                                </ul><!-- /.ul -->
                            </div><!-- /.one-half -->
                        </div><!-- /.content-inner -->
                        <div class="content-inner overflow">
                            <div class="one-half">
                                <div class="image-tabs">
                                    <img class="border-radius-5" src="<?php echo get_template_directory_uri(); ?>/images/page/video-1.jpg" alt="">
                                </div>
                            </div><!-- /.one-half -->
                            <div class="one-half">
                                <div class="text-tabs">
                                    <h5>We are the leader in web development</h5>
                                    <div class="text-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                                    </div>
                                </div><!-- /.text-tabs -->
                            </div><!-- /.one-half -->
                        </div><!-- /.content-inner -->
                        <div class="content-inner overflow">
                            <div class="one-half">
                                <div class="text-tabs">
                                    <h5>We are the leader in web development</h5>
                                    <div class="text-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                                    </div>
                                </div><!-- /.text-tabs -->
                            </div><!-- /.one-half -->
                            <div class="one-half">
                                <ul class="padding-left-100">
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Fully responsive so content looks great</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Awesome sliders to showcase content</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Advanced theme options panel</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Amazing shortcodes loaded with options</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Easy to use Fusion Builder to create pages</li>
                                </ul><!-- /.ul -->
                            </div><!-- /.one-half -->
                        </div><!-- /.content-inner -->
                        <div class="content-inner overflow">
                            <div class="one-half">
                                <div class="text-tabs">
                                    <h5>We are the leader in web development</h5>
                                    <div class="text-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                                    </div>
                                </div><!-- /.text-tabs -->
                            </div><!-- /.one-half -->
                            <div class="one-half">
                                <ul class="padding-left-100">
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Fully responsive so content looks great</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Awesome sliders to showcase content</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Advanced theme options panel</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Amazing shortcodes loaded with options</li>
                                    <li><i class="fa fa-check color-default" aria-hidden="true"></i>Easy to use Fusion Builder to create pages</li>
                                </ul><!-- /.ul -->
                            </div><!-- /.one-half -->
                        </div><!-- /.content-inner -->
                        <div class="content-inner overflow">
                            <div class="one-half">
                                <div class="image-tabs">
                                    <img class="border-radius-5" src="<?php echo get_template_directory_uri(); ?>/images/page/video-1.jpg" alt="">
                                </div>
                            </div><!-- /.one-half -->
                            <div class="one-half">
                                <div class="text-tabs">
                                    <h5>We are the leader in web development</h5>
                                    <div class="text-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                                    </div>
                                </div><!-- /.text-tabs -->
                            </div><!-- /.one-half -->
                        </div><!-- /.content-inner -->
                    </div><!-- /.content-tab -->
                </div><!-- /.tab-about style1 -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-tabs -->