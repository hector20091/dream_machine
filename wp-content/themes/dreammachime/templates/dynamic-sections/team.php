<?php $section = $args['section'];

$title = $section['title'];
$subtitle = $section['subtitle'];
$team_list = $section['team_list']; ?>

<section class="flat-team style3 background">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-title">
                    <h2><?php echo $title; ?></h2>

                    <?php if(!empty($subtitle)) { ?>

                        <p class="sub-title"><?php echo $subtitle; ?></p>

                    <?php } ?>

                </div><!-- /.flat-titles -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->

        <?php if(sizeof($team_list) > 0) { ?>

            <div class="row">

                <?php foreach ($team_list as $team_item) { ?>

                    <div class="col-md-3 col-sm-6">
                        <div class="team-member style4">
                            <div class="avatar">
                                <img src="<?php echo $team_item['photo_url']; ?>">
                            </div>
                            <div class="info">
                                <div class="name"><?php echo $team_item['name']; ?></div>

                                <?php if(!empty($team_item['job_title'])) { ?>

                                    <div class="option">
                                        <?php echo $team_item['job_title']; ?>
                                    </div>

                                <?php } ?>

                            </div>
                        </div><!-- /.team-member style4 -->
                    </div><!-- /.col-md-3 col-sm-6 -->

                <?php } ?>

            </div><!-- /.row -->

        <?php } ?>

    </div><!-- /.container -->
</section><!-- /.flat-team -->