<?php $my_carp_page_id = get_page_id_by_template_name('templates/my-car'); ?>

<section class="flat-actions bg-info">
    <div class="container">
        <form method="post" action="<?php echo get_the_permalink($my_carp_page_id); ?>">
            <div class="call-action style1 v1 bg-transparent">

                <h4 class="color-white text-center">Найти свою машину</h4>
                <div style="padding: 20px 0;text-align: center;">
                    <p style="color: #FFF;">Если вы являетесь одним из наших клиентов и хотите проверить информацию о машине, просто заполните форму ниже:</p>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <input type="text" name="vin" class="form-control" placeholder="Введите 6 последних цифр VIN-кода или № контейнера">
                    </div>
                    <div class="col-lg-3">
                        <button class="bg-white btn-block color-black">Поиск</button>
                    </div>
                </div>

            </div><!-- /.call-action -->
        </form>
    </div><!-- /.container -->
</section>