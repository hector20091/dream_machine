<?php $section = $args['section'];

$content = $section['content_editor']; ?>

<section id="main-blog1" class="single">
    <div class="divider80"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="wrap-post">
                    <article class="blog-post single style1">
                        <div class="content-post">

                            <?php echo $content; ?>

                        </div><!-- /.content-post -->
                    </article><!-- /.blog-post -->
                </div><!-- /.wrap-post -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /#main-blog -->