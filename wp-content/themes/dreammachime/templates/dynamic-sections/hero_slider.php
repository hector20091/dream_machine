<?php $section = $args['section'];

$slides = $section['hero_slides'];

if(!empty($slides) && sizeof($slides) > 0) { ?>

    <section class="tp-banner">
        <div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.3.0.2 auto mode -->
            <div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
                <div class="slotholder"></div>
                <ul><!-- SLIDE  -->

                    <?php foreach ($slides as $key => $slide) {
                        $rs_key = rand(1,10000);
                        $heading_classes = array('tp-caption sub-title ff-2');

                        if(!empty($slide['heading_color'])) {
                            $heading_classes[] = $slide['heading_color'];
                        }

                        ?>

                        <!-- SLIDE <?php echo $key+1; ?> -->
                        <li data-index="rs-<?php echo $rs_key; ?>" data-transition="slideremovedown" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power3.easeInOut" data-easeout="Power3.easeInOut" data-masterspeed="2000"    data-rotate="0"  data-saveperformance="off"  data-title="Ken Burns" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                            <!-- MAIN IMAGE -->
                            <img src="<?php echo $slide['image_url']; ?>"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <!-- LAYER NR. 12 -->
                            <div class="<?php echo join(' ',$heading_classes); ?>"
                                 id="slide-<?php echo $rs_key; ?>-layer-1"
                                 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                 data-y="['middle','middle','middle','middle']" data-voffset="['-16','-16','6','6']"
                                 data-fontsize="['48','45','40','26']"
                                 data-lineheight="['60','60','50','32']"
                                 data-fontweight="['300','300','300','300']"
                                 data-width="100%"
                                 data-height="auto"
                                 data-type="text"
                                 data-whitespace="normal"
                                 data-responsive_offset="on"
                                 data-frames='[{"delay":0,"split":"chars","splitdelay":0.05,"speed":1000,"frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                                 data-textAlign="['center','center','center','center']"
                                 data-paddingright="[0,0,0,0]"
                                 data-paddingbottom="[0,0,0,0"
                                 data-paddingleft="[0,0,0,0]"

                                 style="z-index: 16; white-space: nowrap;font-weight: 300;font-size: 16px;"><?php echo $slide['heading']; ?>
                            </div>

                            <?php if(!empty($slide['show_button'])) {
                                $button_title = $slide['button_title'];
                                $button_url = $slide['button_url']; ?>

                                <!-- LAYER NR. 13 -->
                                <div class="center">
                                    <a href="<?php echo $button_url; ?>" target="_self" class="tp-caption flat-button-slider bg-blue color-white"
                                       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":1200,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'

                                       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                       data-y="['middle','middle','middle','middle']" data-voffset="['83','85','90','90']"

                                       data-fontsize="['12','12','12','12']"
                                       data-lineheight="['46','46','46','44']"
                                       data-fontweight="['600','600','600','600']"
                                       data-textAlign="['center', 'center', 'center', 'center']"
                                       data-width="200"
                                       data-height="46"><?php echo $button_title; ?>
                                    </a><!-- END LAYER LINK -->
                                </div>

                            <?php } ?>

                        </li>

                    <?php } ?>

                </ul>
            </div>
        </div><!-- END REVOLUTION SLIDER -->
    </section><!-- /.tp-banner -->

<?php } ?>