<?php $section = $args['section'];

$title = $section['contact_title'];
$sub_title = $section['conatct_sub_title'];
$description = $section['contact_description'];
$map_addresses = $section['contact_map_addresses'];
$form_shortcode = $section['contact_form_shortcode'];

$description_class = 'col-xs-12';
if(!empty($map_addresses) && sizeof($map_addresses) > 0) {
    $description_class = 'col-md-6';
}

?>

<div class="flat-contact-classic">
    <div class="container">
        <div class="row">
            <div class="<?php echo $description_class; ?>">
                <div class="contact-classic">

                    <?php if(!empty($sub_title)) { ?>

                        <p class="color-default"><?php echo $sub_title; ?></p>

                    <?php } ?>

                    <?php if(!empty($title)) { ?>

                        <h2 class="font-weight-3"><?php echo $title; ?></h2>

                    <?php } ?>

                    <div class="flat-contact contact-page-description">

                        <?php echo $description; ?>

                    </div>

                </div><!-- /.contact-classic -->
            </div><!-- /.col-md-6 -->

            <?php if(!empty($map_addresses) && sizeof($map_addresses) > 0) { ?>

                <div class="col-md-6">
                    <section class="pdmap">
                        <div class="flat-maps" data-address="283 Ashland Rd, Summit, NJ 07901" data-height="370" data-image="images/icons/map.png" data-name="Themesflat Map"></div>

                        <div class="gm-map">
                            <div class="map box-shadow">
                                <div id="contact-map" data-coords='<?php echo json_encode($map_addresses); ?>' style="height: 370px;"></div>
                            </div>
                        </div>
                    </section><!-- /#flat-map -->
                </div><!-- /.col-md-6 -->

            <?php } ?>

        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.flat-contact-classic -->

<?php if(!empty($form_shortcode)) { ?>

    <section class="flat-contact-form style4">
        <div class="container">
            <div class="row">
                <div class="form-contact-form style3 three v3">

                    <?php echo do_shortcode($form_shortcode); ?>

                </div><!-- /.form-contact-form -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-contact-form -->

<?php } ?>