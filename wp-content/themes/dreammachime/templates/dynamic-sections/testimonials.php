<?php global $post;

$testimonials = get_posts(array(
    'post_type' => 'dm_testimonial',
    'post_status' => 'publish',
    'numberposts' => -1
));

if(sizeof($testimonials) > 0) { ?>

    <section class="flat-testimonial">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial owl-carousel-2 style1 v1 no-border-top">

                        <?php foreach ($testimonials as $post) { setup_postdata($post);
                            $company = get_the_excerpt(); ?>

                            <div class="testimonial-item">

                                <?php if(has_post_thumbnail()) { ?>

                                    <div class="avatar">

                                        <?php the_post_thumbnail('middle'); ?>

                                    </div>

                                <?php } ?>

                                <blockquote>
                                    <span>“</span>

                                    <?php the_content(); ?>

                                    <div class="info-author">
                                        <div class="name">
                                            <?php the_title(); ?>
                                        </div>

                                        <?php if(!empty($company)) { ?>

                                            <div class="option">
                                                - <?php echo $company; ?>
                                            </div>

                                        <?php } ?>

                                    </div>
                                </blockquote><!-- /blockquote -->
                            </div><!-- /.testimonial-item -->

                        <?php } wp_reset_postdata(); ?>

                    </div><!-- /.testimonial owl-carousel-1 -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-testimonial -->

<?php } wp_reset_query(); ?>