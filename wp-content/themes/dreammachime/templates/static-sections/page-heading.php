<?php

if(is_front_page()) return false;

$page_id = get_the_ID();

$title = get_the_title();

$style = get_field('heading_style',$page_id);
$background_url = get_field('heading_background_url',$page_id);
$text_color = get_field('heading_text_color',$page_id);

$style = !empty($style)?$style:'simple';

$section_style = array();
$section_classes = array('page-title');
$box_title_classes = array('box-title');

switch ($style) {
    case 'simple':
        $section_classes[] = 'style3';
        $box_title_classes[] = 'style2 color-black';
        break;
    case 'parallax_image':
        $section_classes[] = 'parallax';

        if($text_color == 'white') {
            $box_title_classes[] = 'style1';
        }

        if(!empty($background_url)) {
            $section_style[] = 'background-image: url('.$background_url.')';
        }
        break;
}

?>

<section class="<?php echo join(' ',$section_classes); ?>" style="<?php echo join(';',$section_style); ?>">

    <?php if($style == 'parallax_image') echo '<div class="title-heading">'; ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="<?php echo join(' ',$box_title_classes); ?>">

                    <?php if($style == 'parallax_image') { ?>

                        <h1><?php echo $title; ?></h1>

                    <?php } ?>

                    <?php get_template_part('templates/parts/breadcrumbs'); ?>

                </div><!-- /.box-title -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->

    <?php if($style == 'parallax_image') echo '</div><div class="overlay-black"></div>'; ?>

</section><!-- /.page-title -->