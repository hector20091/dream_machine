<section class="flat-partner background">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="owl-carousel">
                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/partners/AXA_logo-880x660.png"></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/partners/PNGPIX-COM-John-Deere-Logo-PNG-Transparent.png"></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/partners/UBER-BLACK.png"></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/partners/ViDi-Avtomarket.png"></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/partners/dealer-logo.png"></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/partners/atlanta-luxury-motors-where-the-search-ends-85932947.jpg"></li>
                </ul><!-- /.owl-carousel -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-partner background -->