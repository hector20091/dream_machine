<?php /* Template Name: Sections */
get_header(); ?>

<?php get_template_part('templates/static-sections/page-heading'); ?>

<?php get_template_part('templates/parts/pages-sections-loop'); ?>

<?php get_footer();
