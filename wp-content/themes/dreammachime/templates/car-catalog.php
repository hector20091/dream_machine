<?php /* Template Name: Car Catalog */
get_header();

$categories = get_terms('dm_car_catalog_categories');

$cars = get_posts(array(
    'post_type' => 'dm_car_catalog',
    'post_status' => 'publish',
    'numberposts' => -1
));

?>

    <?php get_template_part('templates/static-sections/page-heading'); ?>

    <section class="flat-portfolio style6">

        <?php if(sizeof($categories) > 0) { ?>

            <div class="sort-product style2">
                <ul class="tab-product-list center">
                    <li class="active" data-filter="*">Все</li>

                    <?php foreach ($categories as $cat) { ?>

                        <li data-filter=".<?php echo $cat->slug; ?>"><?php echo $cat->name; ?></li>

                    <?php } ?>

                </ul>
            </div>
            <div class="divider50"></div>

        <?php } ?>

        <?php if(sizeof($cars) > 0) { ?>

            <div class="wrap-portfolio no-padding grid style2">

                <?php foreach ($cars as $post) { setup_postdata($post);
                    $classes = array('portfolio-item color-white one-four left ipsotope');
                    $thumbnail_url = 'http://via.placeholder.com/360x360';
                    $car_categories = wp_get_post_terms(get_the_ID(),'dm_car_catalog_categories');
                    $cat_titles = array();

                    if(sizeof($car_categories) > 0) {
                        foreach ($car_categories as $car_category) {
                            $classes[] = $car_category->slug;
                            $cat_titles[] = $car_category->name;
                        }
                    }

                    if(has_post_thumbnail()) {
                        $thumbnail = get_the_post_thumbnail_url(null, 'full');
                        $thumbnail_url = aq_resize($thumbnail,720,720,true,true,true);
                    } ?>

                    <div class="<?php echo join(' ',$classes); ?>">
                        <div class="portfolio-image">
                            <img src="<?php echo $thumbnail_url; ?>">
                            <div class="portfolio-info center">
                                <h5><a href="#"><?php the_title(); ?></a></h5>
                                <p><?php echo join(', ',$cat_titles); ?></p>
                            </div>
                        </div><!-- /.portfolio-image -->
                    </div><!-- /.portfolio-item -->

                <?php } wp_reset_postdata(); ?>

            </div>

        <?php } wp_reset_query(); ?>

    </section>

<?php get_footer(); ?>