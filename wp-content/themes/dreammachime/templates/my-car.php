<?php /* Template Name: My Car */

if(isset($_POST['vin'])) {
    get_template_part('templates/my-car-tpl');
} else {
    wp_redirect(get_site_url());
}