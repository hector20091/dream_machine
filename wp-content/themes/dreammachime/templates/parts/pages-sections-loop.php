<?php $sections = get_field('page_sections');

if(!empty($sections)) {
    foreach ($sections as $section) {
        $type = $section['acf_fc_layout'];
        $templates_path = 'templates/dynamic-sections/' . $type;

        if(isset($_GET['debug']) && $_GET['debug'] == 1) {
            echo '<pre>';print_r('Template: ' . $templates_path);echo '</pre>';
        }

        if (locate_template($templates_path . '.php') != '') {
            theme_get_template($templates_path, array(
                'section' => $section
            ));
        }
    }
} ?>