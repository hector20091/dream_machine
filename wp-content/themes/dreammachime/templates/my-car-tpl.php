<?php get_header();

$car_id = get_post_id_by_value($_POST['vin']);

$title = get_the_title($car_id);

$delivery_info = get_mycar_delivery($car_id);
$car_info = get_mycar_info($car_id);

$photos = get_field('cc_make_photos',$car_id); ?>

<?php get_template_part('templates/static-sections/page-heading'); ?>

<section class="flat-portfolio-single">
    <div class="container">
        <div class="single-simple margin-bottom-30">
            <h3><?php echo $title; ?></h3>
        </div><!-- /.single-simple -->

        <div class="row">
            <div class="col-md-7">

                <?php if(sizeof($photos)) { ?>

                    <ul class="owl-carousel-10 owl-my-car-photos">

                        <?php foreach ($photos as $photo_data) { ?>

                            <li>
                                <img src="<?php echo $photo_data['url']; ?>">
                            </li>

                        <?php } ?>

                    </ul><!-- /.owl-carousel-8 -->

                <?php } ?>

            </div><!-- /.col-md-12 -->
            <div class="col-md-5">

                <?php if(sizeof($delivery_info) > 0) { ?>

                    <div class="car-info-box">
                        <h5>Информация о транспортровке</h5>
                        <table>
                            <tbody>

                            <?php foreach ($delivery_info as $title => $value) { ?>

                                <tr>
                                    <td><?php echo $title; ?></td>
                                    <td><?php echo $value; ?></td>
                                </tr>

                            <?php } ?>

                            </tbody>
                        </table>
                    </div>

                <?php } ?>

                <div class="divider30"></div>

                <?php if(sizeof($car_info) > 0) { ?>

                    <div class="car-info-box">
                        <h5>Информация о машине</h5>
                        <table>
                            <tbody>

                            <?php foreach ($car_info as $title => $value) { ?>

                                <tr>
                                    <td><?php echo $title; ?></td>
                                    <td><?php echo $value; ?></td>
                                </tr>

                            <?php } ?>

                            </tbody>
                        </table>
                    </div>

                <?php } ?>

            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-portfolio-slide -->

<?php
wp_reset_postdata();
wp_reset_query();

get_footer(); ?>