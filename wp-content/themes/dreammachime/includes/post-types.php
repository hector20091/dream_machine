<?php

function client_cars_post_type_init() {
    $args = array(
        'labels'             => array(
            'name'          => __('Авто Клиентов'),
            'singular_name' => __('Авто Клиента')
        ),
        'public'             => true,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => false,
        'rewrite'            => array('slug' => 'client-cars'),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title')
    );

    register_post_type('dm_client_car', $args);
}

function car_catalog_post_type_init() {
    $args = array(
        'labels' => array(
            'name' => __( 'Каталог Авто' ),
            'singular_name' => __( 'Авто' )
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'car-catalog' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail')
    );

    register_post_type( 'dm_car_catalog', $args );

    register_taxonomy(
        'dm_car_catalog_categories',
        'dm_car_catalog',
        array(
            'label' => __( 'Категории' ),
            'rewrite' => array( 'slug' => 'car-catalog-cat' ),
            'hierarchical' => true,
        )
    );
}

function testimonials_post_type_init() {
    $args = array(
        'labels'             => array(
            'name'          => __('Отзывы'),
            'singular_name' => __('Отзыв')
        ),
        'public'             => true,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => false,
        'rewrite'            => array('slug' => 'testimonials'),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array('title','editor','thumbnail','excerpt')
    );

    register_post_type('dm_testimonial', $args);
}

add_action('init','client_cars_post_type_init');
add_action('init','car_catalog_post_type_init');
add_action('init','testimonials_post_type_init');