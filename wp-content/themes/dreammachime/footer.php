<?php

$social_buttons = get_field('social_buttons','option');
$copyright = get_field('copyright','option');

?>


    <?php get_template_part('templates/static-sections/partners'); ?>

    <footer class="style2 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="widget-about">
                        <div class="logo-ft">
                            <a href="<?php bloginfo('url') ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.jpg" width="60">
                            </a>
                        </div>

                        <?php if(!empty($social_buttons)) { ?>

                            <ul class="social-ft">

                                <?php foreach ($social_buttons as $button) {
                                    $icon_classes = array('fa');

                                    if(!empty($button['css_class'])) {
                                        $icon_classes[] = $button['css_class'];
                                    } ?>

                                    <li>
                                        <a href="<?php echo $button['url']; ?>" target="_blank">
                                            <i class="<?php echo join(' ',$icon_classes); ?>" aria-hidden="true"></i>
                                        </a>
                                    </li>

                                <?php } ?>

                            </ul><!-- /.social-ft -->

                        <?php } ?>

                        <?php if(!empty($copyright)) { ?>

                            <p><?php echo $copyright; ?></p>

                        <?php } ?>

                    </div><!-- /.widget-about -->
                </div><!-- /.col-md-4 -->

                <?php if ( is_active_sidebar( 'footer-menus' ) ) { ?>

                    <div class="col-md-4">
                        <div class="widget-menu-ft">

                            <?php dynamic_sidebar( 'footer-menus' ); ?>

                        </div><!-- /.widget-menu-ft -->
                    </div><!-- /.col-md-4 -->

                <?php } ?>

                <?php if ( is_active_sidebar( 'footer-info' ) ) { ?>

                    <div class="col-md-4">

                        <?php dynamic_sidebar( 'footer-info' ); ?>

                    </div><!-- /.col-md-4 -->

                <?php } ?>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </footer><!-- /.footer style2 -->

    <div class="button-go-top">
        <a href="#" title="" class="go-top">
            <i class="fa fa-chevron-up"></i>
        </a><!-- /.go-top -->
    </div><!-- /.button-go-top -->

</div><!-- /.boxed -->

<?php wp_footer(); ?>

</body>
</html>
