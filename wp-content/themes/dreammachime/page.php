<?php get_header(); ?>

<?php get_template_part('templates/static-sections/page-heading'); ?>


    <section id="main-blog1" class="single">
        <div class="divider80"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrap-post">

                        <?php while ( have_posts() ) : the_post(); ?>

                            <article class="blog-post single style1">
                                <div class="content-post">

                                    <?php the_content(); ?>

                                </div><!-- /.content-post -->
                            </article><!-- /.blog-post -->

                        <?php endwhile; ?>

                    </div><!-- /.wrap-post -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /#main-blog -->

<?php get_footer();
