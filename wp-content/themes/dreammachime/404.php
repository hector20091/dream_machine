<?php get_header(); ?>

    <?php get_template_part('templates/static-sections/page-heading'); ?>

    <section class="flat-error">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrap-error center">
                        <div class="header-error">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/02441267_n1.png" alt="">
                        </div>
                        <div class="content-error">
                            <h3 class="font-weight-3"><?php _e( 'К сожалению! Эта страница не может быть найдена.', 'dreammachine' ); ?></h3>
                            <p class="font-size-16"><?php _e('К сожалению, страница, которую вы ищете, не существует, была удалена или имя изменено','dreammachine') ?></p>
                            <div class="btn-more">
                                <a href="<?php bloginfo('url'); ?>" class="base border-radius-2 color-white background-blue"><?php _e('Перейти на Главную','dreammachine'); ?></a>
                            </div>
                        </div><!-- /.content-error -->
                    </div><!-- /.wrap-error -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-error -->

<?php get_footer();
