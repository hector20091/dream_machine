<?php

include "includes/aq_resizer.php";
include "includes/post-types.php";

/**
 * Dream Machine functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Dream_Machine
 * @since 1.0
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function dreammachine_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/dreammachine
	 * If you're building a theme based on Dream Machine, use a find and replace
	 * to change 'dreammachine' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'dreammachine' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	//add_image_size( 'dreammachine-featured-image', 2000, 1200, true );

	//add_image_size( 'dreammachine-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'dreammachine' ),
		'social' => __( 'Social Links Menu', 'dreammachine' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'dreammachine_setup' );

/**
 * Add preconnect for Google Fonts.
 *
 * @since Dream Machine 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function dreammachine_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'dreammachine-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'dreammachine_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function dreammachine_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'dreammachine' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'dreammachine' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Футер Меню', 'dreammachine' ),
		'id'            => 'footer-menus',
		'before_widget' => '<div id="%1$s" class="widget %2$s one-half">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Футер - Информация', 'dreammachine' ),
		'id'            => 'footer-info',
		'before_widget' => '<div id="%1$s" class="widget %2$s widget-information">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'dreammachine_widgets_init' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Dream Machine 1.0
 */
function dreammachine_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'dreammachine_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function dreammachine_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'dreammachine_pingback_header' );

/**
 * Enqueue scripts and styles.
 */
function dreammachine_scripts() {
    wp_deregister_script('jquery');
    wp_deregister_script('google-maps');

    wp_register_script('google-maps', 'https://maps.google.com/maps/api/js?sensor=true&key=' . get_google_maps_api_key());
    wp_register_script('jquery',get_theme_file_uri('/javascript/jquery.min.js'));
    wp_register_script('tether',get_theme_file_uri('/javascript/tether.min.js'),array('jquery'),false,true);
    wp_register_script('bootstrap',get_theme_file_uri('/javascript/bootstrap.min.js'),array('jquery'),false,true);
    wp_register_script('waypoints',get_theme_file_uri('/javascript/waypoints.min.js'),array('jquery'),false,true);
    wp_register_script('easing',get_theme_file_uri('/javascript/easing.js'),array('jquery'),false,true);
    wp_register_script('fancybox',get_theme_file_uri('/javascript/jquery.fancybox.js'),array('jquery'),false,true);
    wp_register_script('kinetic',get_theme_file_uri('/javascript/kinetic.js'),array('jquery'),false,true);
    wp_register_script('flexslider',get_theme_file_uri('/javascript/jquery.flexslider-min.js'),array('jquery'),false,true);
    wp_register_script('owl-carousel',get_theme_file_uri('/javascript/owl.carousel.js'),array('jquery'),false,true);
    wp_register_script('parallax',get_theme_file_uri('/javascript/parallax.js'),array('jquery'),false,true);
    wp_register_script('imagesloaded',get_theme_file_uri('/javascript/imagesloaded.pkgd.min.js'),array('jquery'),false,true);
    wp_register_script('isotope',get_theme_file_uri('/javascript/isotope.pkgd.min.js'),array('jquery'),false,true);
    wp_register_script('final-countdown',get_theme_file_uri('/javascript/jquery.final-countdown.js'),array('jquery'),false,true);
    wp_register_script('theme-main',get_theme_file_uri('/javascript/main.js'),array('jquery'),false,true);
    wp_register_script('revolution-tools',get_theme_file_uri('/revolution/js/jquery.themepunch.tools.min.js'),array('jquery'),false,true);
    wp_register_script('revolution',get_theme_file_uri('/revolution/js/jquery.themepunch.revolution.min.js'),array('jquery'),false,true);
    wp_register_script('revolution-slider',get_theme_file_uri('/revolution/js/slider.js'),array('jquery'),false,true);
    wp_register_script('dm-theme',get_theme_file_uri('/js/dm-theme.js'),array('jquery'),false,true);

    // Scripts
    wp_enqueue_script('google-maps');
    wp_enqueue_script('jquery');
    wp_enqueue_script('tether');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('waypoints');
    wp_enqueue_script('easing');
    wp_enqueue_script('fancybox');
    wp_enqueue_script('kinetic');
    wp_enqueue_script('flexslider');
    wp_enqueue_script('owl-carousel');
    wp_enqueue_script('parallax');
    wp_enqueue_script('imagesloaded');
    wp_enqueue_script('isotope');
    wp_enqueue_script('final-countdown');
    wp_enqueue_script('theme-main');
    wp_enqueue_script('revolution-tools');
    wp_enqueue_script('revolution');
    wp_enqueue_script('revolution-slider');
    wp_enqueue_script('dm-theme');

	// Styles
	wp_enqueue_style( 'bootstrap', get_theme_file_uri('/stylesheets/bootstrap.min.css'));
	wp_enqueue_style( 'fancybox', get_theme_file_uri('/stylesheets/fancybox.css'));
	wp_enqueue_style( 'rev-slider-layers', get_theme_file_uri('/revolution/css/layers.css'));
	wp_enqueue_style( 'rev-slider-settings', get_theme_file_uri('/revolution/css/settings.css'));
	wp_enqueue_style( 'theme-style', get_theme_file_uri('/stylesheets/style.css'));
	wp_enqueue_style( 'theme-responsive-style', get_theme_file_uri('/stylesheets/responsive.css'));
	wp_enqueue_style( 'theme-colors', get_theme_file_uri('/stylesheets/colors/color1.css'));

	// Theme stylesheet.
	wp_enqueue_style( 'dreammachine-style', get_stylesheet_uri() );

    wp_localize_script( 'dm-theme', 'dmConfig', array(
        'ajaxUrl' => admin_url('admin-ajax.php'),
        'map_marker' => get_template_directory_uri() . '/images/icons/map.png'
    ) );
}
add_action( 'wp_enqueue_scripts', 'dreammachine_scripts' );

function dm_body_class( $classes ) {
    $classes[] = 'header_sticky';

    return $classes;
}
add_filter('body_class','dm_body_class');

function theme_get_template( $template = '', $args = array(), $echo = true ) {
    ob_start();
    include TEMPLATEPATH . '/' . $template . '.php';
    $content = ob_get_contents();
    ob_end_clean();

    if($echo) {
        echo $content;
    } else {
        return $content;
    }
}

function get_page_id_by_template_name($template = '',$all = false) {
    if(!empty($template)) {
        $template = $template.'.php';
        $page_info = get_posts(array(
            'post_type'   => 'page',
            'meta_query' => array(
                array(
                    'key' => '_wp_page_template',
                    'value' => $template
                )
            )
        )) ;

        wp_reset_query();
        wp_reset_postdata();

        if($all) {
            $parents = array();

            if(sizeof($page_info) > 0) {
                foreach ($page_info as $page_info_single) {
                    $parents[] = $page_info_single->ID;
                }

            }

            return $parents;
        } else {
            return $page_info[0]->ID;
        }
    }
}

function get_post_id_by_value($value = '') {
    global $wpdb;

    if(!empty($value)) {
        $results = $wpdb->get_results( "select post_id, meta_key from $wpdb->postmeta where meta_value = '".$value."'", ARRAY_A );

        if($results[0]['post_id']) {
            return $results[0]['post_id'];
        }
    }

    return false;
}

function get_mycar_info($car_id = null) {
    $data = array();

    if(is_null($car_id)) return $data;

    $make_vin = get_field('cc_vin_number',$car_id);
    $make = get_field('cc_make',$car_id);
    $make_model = get_field('cc_model',$car_id);
    $make_year = get_field('cc_make_year',$car_id);
    $make_engine_volume = get_field('сс_make_engine_volume',$car_id);
    $make_mileage = get_field('cc_make_mileage',$car_id);

    if(!empty($make)) {
        $data['Марка'] = $make;
    }
    if(!empty($make_model)) {
        $data['Модель'] = $make_model;
    }
    if(!empty($make_year)) {
        $data['Дата производства'] = $make_year;
    }
    if(!empty($make_engine_volume)) {
        $data['Объем двигателя'] = $make_engine_volume;
    }
    if(!empty($make_mileage)) {
        $data['Пробег'] = $make_mileage;
    }
    if(!empty($make_mileage)) {
        $data['VIN'] = $make_vin;
    }

    return $data;
}

function get_mycar_delivery($car_id = null) {
    $data = array();

    if(is_null($car_id)) return $data;

    $status_data = get_field('cc_status',$car_id);
    $container_number = get_field('cc_container_number',$car_id);
    $transporter = get_field('cc_transporter',$car_id);
    $transporter_url = get_field('cc_transporter_url',$car_id);
    $arrival_date = get_field('cc_arrival_date',$car_id);

    if(!empty($status_data)) {
        $data['Статус'] = $status_data['label'];
    }

    if(!empty($status_data) && $status_data['value'] == 'container' && !empty($container_number)) {
        $data['№ Контейнера'] = $container_number;
    }

    if(!empty($transporter)) {
        $data['Перевозчик'] = $transporter;
    }

    if(!empty($arrival_date)) {
        $data['Дата прибытия в Одессу'] = $arrival_date;
    }

    return $data;
}

function get_google_maps_api_key() {
    return 'AIzaSyCGmuFkc3DKoE1AW_Uijq4T2w7X6NcgwSs';
}

add_filter('acf/settings/google_api_key', function () {
    return get_google_maps_api_key();
});

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}